
from django.conf.urls import url
from views import about_us, learn, show_privacy, show_tnc, show_faqs, ContactUsView

urlpatterns = [
    url(r'^aboutus/$', about_us, name="about_us"),
    url(r'^learn/$', learn, name="learn"),
url(r'^privacy-policy/$', show_privacy , name="privacy"),
url(r'^terms-and-conditions/$', show_tnc , name="terms"),
url(r'^faqs/$', show_faqs , name="faqs"),
url(r'^contact-us/$', ContactUsView.as_view() , name="contact-us"),

]