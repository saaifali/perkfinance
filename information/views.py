# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from django.conf import settings
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.mail import send_mail, EmailMessage
from models import Query

# Create your views here.
def about_us(request):
    return render(request, "information/aboutus.html")

def learn(request):
    return  render(request, "information/pf101.html")


def show_privacy(request):
    return render(request, "information/privacypol.html")


def show_tnc(request):
    return render(request, "information/terms.html")


def show_faqs(request):
    return render(request, "information/faqs1.html")

class ContactUsView(TemplateView):
    template_name = "information/contactus.html"

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        os.environ['DJANGO_SETTINGS_MODULE'] = 'perkfinance.settings'
        first_name = request.POST.get("firstname", None)
        last_name = request.POST.get("lastname", None)
        query = request.POST.get("comment", None)
        email = request.POST.get("email", None)
        phone = request.POST.get("mobnum", 0)
        try:
            new_message = Query(first_name=first_name, last_name=last_name, message=query, email=email, number = phone)
            new_message.save()
            subject = 'Hassle Free solutions to your queries!'
            message = "We have registered your concern and will get back to you shortly! Thank you!\nTry it out at www.perkfinance.com!"
            from_email = settings.EMAIL_HOST_USER
            to_list = [email]
            email = EmailMessage(
                subject= subject,
                body=message,
                from_email=from_email,
                to=to_list,
                reply_to=[from_email],
                headers={'Content-Type': 'text/plain'},
            )
            print email.send()
            #print send_mail(subject=subject, message=message, from_email=from_email, recipient_list=to_list,fail_silently=True)


        except:
            data = {
                "error" : "Component under maintainance. Please try again later"
            }
            return render(request, self.template_name, data)
        return redirect("index")