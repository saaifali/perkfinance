# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Query

# Register your models here.


class QueryAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'message', 'number')


admin.site.register(Query, QueryAdmin)