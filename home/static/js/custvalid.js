window.Parsley.addValidator('alphadash', {
  validateString: function(value) {
    return true == (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{3,}))$/.test(value));
  }

});

window.Parsley.addValidator('mob', {
  validateString: function(value) {
    return true == (/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(value));
  }

});



/* JS FOR 2ND FORM*/
window.Parsley.addValidator('alpha', {
  validateString: function(value) {
    return true == (/^[a-zA-Z]+$/.test(value));
  }

});


window.Parsley.addValidator('dater', {
  validateString: function(value) {
    return true == (/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}\/?$/.test(value));
  }

});

window.Parsley.addValidator('pin', {
  validateString: function(value) {
    return true == (/^[1-9][0-9]{5}$/.test(value));
  }

});

window.Parsley.addValidator('city', {
  validateString: function(value) {
    return true == (/^[a-zA-Z\s]+$/.test(value));
  }

});
