# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import EmailList

# Register your models here.
class RefererAdmin(admin.ModelAdmin):
    list_display = ('referer',)


admin.site.register(EmailList, RefererAdmin)