from django.conf.urls import url
from home.views import HomeView, ReferView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name = "index"),
url(r'^refer/$', ReferView.as_view(), name = "refer"),
]
