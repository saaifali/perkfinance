
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import os
from django.conf import settings
from django.core.mail import send_mail, EmailMessage
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from models import EmailList

# Create your views here.

class HomeView(TemplateView):
    template_name = 'home/index.html'

    def get(self, request, *args, **kwargs):
        session = request.session.get("exists", None)
        data = {}
        if session=="False":
            data = { "session_expired": 1}
        request.session["exists"] = "home"
        return render(request, self.template_name, data)



class ReferView(TemplateView):
    template_name = 'home/refer.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})

    def post(self, request):
        email = request.POST.get("email", None)
        if email is not None:
            try:
                EmailList.objects.get(referer=email)
            except:
                new_referer = EmailList(referer=email)
                new_referer.save()

        friend_email = request.POST.get("email2",None)
        os.environ['DJANGO_SETTINGS_MODULE'] = 'perkfinance.settings'
        subject = 'Hassle Free Loans a click away'
        message = "Your friend at "+email+" has given Perk Finance a try and would like to share it with you.\nTry it out at www.perkfinance.com!"
        from_email = settings.EMAIL_HOST_USER
        to_list = [friend_email, settings.EMAIL_HOST_USER]
        email = EmailMessage(
            subject=subject,
            body=message,
            from_email=from_email,
            to=to_list,
            reply_to=[from_email],
            headers={'Content-Type': 'text/plain'},
        )
        print email.send()
        return redirect("index")


def view_sitemap(request):
    return render(request, "sitemap.xml", {})