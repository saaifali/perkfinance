from django.conf.urls import include, url
from register.views import RegisterView, GetInfoView, show_success, check_info, show_result, show_sorry, send_otp, ProfileView, EditProfileView, verify, activate
from register.api.views import SignupView, SetPinUserView, LoginUserView, UserDetailView
from rest_framework.authtoken import views

urlpatterns = [
    url(r'^$', RegisterView.as_view() , name = "register"),
    url(r'^get_info/$', GetInfoView.as_view() , name = "get_information"),
    url(r'^check/$', check_info , name = "check"),
    url(r'^result/$', show_result , name = "result"),
    url(r'^sorry/$', show_sorry, name = 'sorry'),
    url(r'^success/$', show_success, name = 'success'),
    url(r'^ajax/verify-otp/$', send_otp, name = 'send_otp'),
    url(r'^profile/$', ProfileView.as_view(), name = 'profile'),
    url(r'^edit-profile/$', EditProfileView.as_view(), name = 'edit_profile'),
    url(r'^verify/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        verify, name='verify'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),
    url(r'^api/check_user', SignupView.as_view(), name = 'api_sign_up'),
    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^api/login/set_pin', SetPinUserView.as_view(), name = 'api_login_set_pin'),
    url(r'^api/login',LoginUserView.as_view(), name='api_login'),
    url(r'^api/user-profile',UserDetailView.as_view(), name='api_user_profile'),
]
