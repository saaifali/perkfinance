# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from rest_framework.authtoken.models import Token
# Create your models here.



class InfoModel(models.Model):
    phone_number = models.BigIntegerField(default=0, unique=True, primary_key=True)
    email = models.EmailField(max_length=254, null=True, unique=True)
    email_verified = models.BooleanField(default=False)
    first_name = models.CharField(max_length=100, default="")
    last_name = models.CharField(max_length=100, default="")
    gender = models.CharField( max_length=10, null=True)
    dob = models.DateField(null=True)
    pin = models.IntegerField( null=True)
    area = models.CharField( max_length=50, null=True)
    city = models.CharField( max_length=50, null=True)
    income = models.CharField(max_length=20, null=True)
    net_income = models.IntegerField(default=-1)
    approved = models.BooleanField(default=False)
    amount_approved = models.IntegerField(default=-1)



class AppProfile(models.Model):
    #user = models.ForeignKey(unique=True, null=False)
    #profile_image = models.ImageField(null=True)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    gender = models.CharField(max_length=10, null=True)
    dob = models.DateField(null=True)
    pin = models.IntegerField(null=True)
    address = models.CharField(max_length=50, null=True)
    area = models.CharField(max_length=50, null=True)
    city = models.CharField(max_length=50, null=True)
    residing_since = models.DateField(null=True)
    home_type = models.CharField(max_length=100, null=True)
    education = models.CharField(max_length=100, null=True)
    college = models.CharField(max_length=100, null=True)
    college_city = models.CharField(max_length=100, null=True)
    income_type = models.CharField(max_length=100, null = True)
    total_experience = models.IntegerField(null = True)
    monthly_salary = models.IntegerField(default=-1, null=True)
    current_emi = models.IntegerField(null=True)
    company = models.CharField(max_length=100, null=True)
    company_designation = models.CharField(max_length=100, null=True)
    company_pin = models.IntegerField(null=True)
    company_address = models.CharField(max_length=50, null=True)
    company_area = models.CharField(max_length=50, null=True)
    company_city = models.CharField(max_length=50, null=True)


class PerkUser(models.Model):
    user = models.OneToOneField(User, null=False, on_delete=models.CASCADE)
    web_info = models.OneToOneField(InfoModel, null=True)
    app_profile_info = models.OneToOneField(AppProfile, null=True)





@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        #profile = InfoModel.objects.create()
        web_profile = InfoModel.objects.create(email=instance.email)
        perkuser = PerkUser.objects.create(user=instance, web_info=web_profile)
        perkuser.save()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)