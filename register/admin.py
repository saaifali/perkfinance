# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import InfoModel, PerkUser

# Register your models here.
class InfoModelAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'phone_number')

class PerkUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'web_info')


admin.site.register(InfoModel, InfoModelAdmin)
admin.site.register(PerkUser, PerkUserAdmin)

