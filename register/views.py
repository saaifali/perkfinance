# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import sendotp
import random
import math
import locale
import re
from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect, HttpResponse
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from register.models import InfoModel

from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode,urlsafe_base64_decode
from django.template.loader import render_to_string
from register.tokens import account_activation_token, email_verification_token
from django.contrib.auth import login
from django.contrib.auth.models import User


MONTHS = {
    "Jan" : "01",
    "Feb" : "02",
    "Mar" : "03",
    "Apr" : "04",
    "May" : "05",
    "Jun" : "06",
    "Jul" : "07",
    "Aug" : "08",
    "Sep" : "09",
    "Oct" : "10",
    "Nov" : "11",
    "Dec" : "12"
}

auth = {
    "8008133355",
}


#Functions
def convert_to_database_dob(dob):
    matches = re.match(r'([\d]{2})/([\d]{2})/([\d]{4})[/]?', dob)
    db_dob = ''
    if matches:
        year = matches.group(3)
        date = matches.group(1)
        month = matches.group(2)
        db_dob = year + "-" + month + "-" + date
    return db_dob

def convert_to_form_dob(dob):
    matches = re.match(r'([\d]{4})-([\d]{2})-([\d]{2})', dob)
    form_dob = ''
    if matches:
        year = matches.group(1)
        date = matches.group(3)
        month = matches.group(2)
        form_dob = date + "/" + month + "/" + year
    return form_dob


@csrf_exempt
def send_otp(request):
    if request.method == 'GET':
        print request.GET
        phone = request.GET.get("phone", None)
        admin = False
        if str(phone) in auth:
            admin = True
        mobile = "91"+str(phone)
        mobile = int(mobile)
        otp = random.randrange(1000, 9999)
        request.session["otp"] = str(otp)
        print "otp : " + str(otp)
        if not admin:

            otpobj = sendotp.sendotp(settings.OTP_AUTH_KEY, 'Greetings from Perk Finance!\n Your otp is {{otp}} .')

            print otpobj.send(mobile, 'PERKFI', otp)

            success = otpobj.verify(mobile, otp)

            #if success != 200:
            #    otpobj.retry(mobile, 'voice')
            #data = {
            #    'sent' : True
            #}
            #if not data['sent']:
             #   data['error_message'] = 'Maintainance going on. Please try later!'
        return HttpResponse('OK')


# Create your views here.
def check_info(request):
    if (request.session.get("exists", "False")) == "False":
        return redirect("index")
    return render(request, "register/check_eligibility.html")


def show_result(request):
    email = request.session.get("email", None)
    phone = request.session.get("number", None)
    net_income = int(request.session.get("net_income", None))

    if net_income>=25000:
        return redirect("success")

    return redirect("sorry")



def show_sorry(request):
    if (request.session.get("exists","False")) == "False":
        return redirect("index")
    request.session['exists'] = "False"
    #del request.session['email']
    #del request.session['number']
    #del request.session['net_income']
    return render(request, "register/sorry.html")


def show_success(request):
    if (request.session.get("exists", "False")) == "False":
        return redirect("index")
    locale.setlocale(locale.LC_ALL, '')
    email = request.session.get("email", None)
    phone = request.session.get("number", None)
    print phone
    print email
    approved = 0
    try:
        instance = InfoModel.objects.get(email=email, phone_number=phone)
        approved = instance.amount_approved
    except:
        return HttpResponse("Database Error!")
    data = {"amount": locale.format("%d", approved, grouping=True) }
    request.session['exists'] = "False"
    #del request.session['email']
    #del request.session['number']
    #del request.session['net_income']
    return render(request, "register/success.html", data)



class RegisterView(TemplateView):
    template_name = 'register/register_one.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request):
        self.email = request.POST.get('email')
        self.email = self.email.lower()
        self.number = request.POST.get('mobnum')
        self.otp = request.POST.get('otp')
        #assign to the current user session
        request.session["email"] = self.email
        request.session["number"] = str(self.number)
        request.session["exists"] = "True"

        self.generatedOTP = request.session.get('otp')

        if(self.otp != self.generatedOTP):
            return render(request, self.template_name, {'error_msg': "OTP verification failed!"})

        print "here",self.email, self.number

        try:
            InfoModel.objects.get(phone_number=int(self.number), email=self.email)
            return redirect("profile")
        except InfoModel.DoesNotExist:
            try:
                InfoModel.objects.get(phone_number=int(self.number))
                number_exists = 1
                return render(request, self.template_name, {'number_exists':number_exists})
            except InfoModel.DoesNotExist:
                pass
            try:
                InfoModel.objects.get(email=self.email)
                email_exists = 1
                return render(request, self.template_name, {'email_exists': email_exists})
            except InfoModel.DoesNotExist:
                pass
        #self.verify_with_otp()
        return redirect('get_information')




class GetInfoView(TemplateView):
    template_name = "register/register_two.html"

    def get(self, request, *args, **kwargs):
        if request.session["exists"] == "True":
            return render(request, self.template_name)
        else:
            return redirect("register")


    def post(self, request):
        self.email = request.session.get('email')
        self.number = request.session.get('number')
        self.first_name = request.POST.get('firstname')
        self.last_name = request.POST.get('lastname')
        self.gender = request.POST.get('gender')
        date = str(request.POST.get('date'))
        self.dob = convert_to_database_dob(date)
        self.pin = request.POST.get('address')
        self.area = request.POST.get('area')
        self.city = request.POST.get('city')
        self.income_type = request.POST.get('sa')
        self.net = int(request.POST.get('ni'),0)
        if self.net=="":
            self.net = 0 
        request.session["net_income"] = self.net
        self.is_approved = False
        self.approved = 0
        self.find_approved()

        print self.email, self.number, self.last_name, self.gender, self.dob, self.pin, self.area, self.city, self.income_type, self.net, self.is_approved, self.approved
        if self.validate():
            try:
                new_entry = InfoModel(phone_number=self.number, email=self.email, first_name= self.first_name, last_name=self.last_name, gender= self.gender, dob=self.dob, pin=self.pin, area=self.area, city=self.city, income=self.income_type, net_income=self.net, approved = self.is_approved, amount_approved = self.approved)
                new_entry.save()
                verify_email(request=request, email=self.email, infomodel=new_entry)
            except:
                print "Database entry error!"

            return redirect('check')
        else:
            return render(request, self.template_name)

    # Logic for pre-approval
    def find_approved(self):
        self.is_approved = False
        self.approved = 0
        if self.net >= 25000:
            self.is_approved = True
            amount = float(self.net) * 0.6
            amount = amount / 10000
            amount = math.floor(amount)
            amount = amount * 10000
            if amount > 100000:
                amount = 100000
            self.approved = int(amount)
        return

    def validate(self):
        return True
        #if re.match(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', self.email) == None:




#Code to show your current profile data
class ProfileView(TemplateView):
    template_name = "register/profile.html"

    def get(self, request, *args, **kwargs):
        locale.setlocale(locale.LC_ALL, '')

        self.email = request.session.get('email')
        self.number = request.session.get('number')

        try:
            user = InfoModel.objects.get(phone_number=int(self.number), email=self.email)
        except:
            return redirect("index")

        if user.gender == "M":
            gender = "Male"
        else:
            gender = "Female"

        profile_data = {
            "email": user.email,
            "number": user.phone_number,
            "first": user.first_name+" ",
            "last": user.last_name,
            "gender": gender,
            "dob": convert_to_form_dob(str(user.dob)),
            "pin": user.pin,
            "area": user.area,
            "city": user.city,
            "income_type": user.income,
            "net_income": locale.format("%d", user.net_income, grouping=True),
            "approved": user.approved,
            "amount_approved": locale.format("%d",user.amount_approved, grouping=True)
        }
        return render(request,self.template_name, profile_data)



#Code to edit your profile.
class EditProfileView(TemplateView):
    template_name = "register/edit_profile.html"

    def get(self, request, *args, **kwargs):
        self.email = request.session.get('email')
        self.number = request.session.get('number')
        try:
            user = InfoModel.objects.get(phone_number=int(self.number), email=self.email)
        except:
            return HttpResponse("Database Error!")
        print user.dob
        profile_data = {
            "email": user.email,
            "number": user.phone_number,
            "first": user.first_name,
            "last": user.last_name,
            "gender": user.gender,
            "dob": convert_to_form_dob(str(user.dob)),
            "pin": user.pin,
            "area": user.area,
            "city": user.city,
            "income_type": user.income,
            "net_income": user.net_income,
            "approved": user.approved,
            "amount_approved": user.amount_approved
        }
        print profile_data
        return render(request,self.template_name, profile_data)

    def post(self, request):
        self.email = request.session.get('email')
        self.number = request.session.get('number')
        self.first_name = request.POST.get('firstname')
        self.last_name = request.POST.get('lastname')
        self.gender = request.POST.get('gender')
        date = str(request.POST.get('date'))
        self.dob = convert_to_database_dob(date)
        self.pin = request.POST.get('address')
        self.area = request.POST.get('area')
        self.city = request.POST.get('city')
        self.income_type = request.POST.get('sa')
        self.net = int(request.POST.get('ni'), 0)
        if self.net=="":
            self.net = 0 
        request.session["net_income"] = self.net
        self.is_approved = False
        self.approved = 0
        self.find_approved()

        profile_data = {
            "email": self.email,
            "number": self.number,
            "first": self.first_name,
            "last": self.last_name,
            "gender": self.gender,
            "dob": convert_to_form_dob(str(self.dob)),
            "pin": self.pin,
            "area": self.area,
            "city": self.city,
            "income_type": self.income_type,
            "net_income": self.net,
            "approved": 1 if self.is_approved else 0,
            "amount_approved": self.approved
        }

        print self.email, self.number,self.first_name, self.last_name, self.gender, self.dob, self.pin, self.area, self.city, self.income_type, self.net, self.is_approved, self.approved
        if self.validate():
            try:
                user = InfoModel.objects.get(phone_number=int(self.number), email=self.email)
                user.first_name = self.first_name
                user.last_name = self.last_name
                user.gender = self.gender
                user.dob = self.dob
                user.pin = self.pin
                user.area = self.area
                user.city = self.city
                user.income = self.income_type
                user.net_income = self.net
                user.approved = self.is_approved
                user.amount_approved = self.approved
                user.save()
                print user
            except:
                print "Database modification error!"
            return redirect('check')
        else:
            return render(request, self.template_name, profile_data)


    # Logic for pre-approval
    def find_approved(self):
        self.is_approved = False
        self.approved = 0
        if self.net >= 25000:
            self.is_approved = True
            amount = float(self.net) * 0.6
            amount = amount / 10000
            amount = math.floor(amount)
            amount = amount * 10000
            if amount > 100000:
                amount = 100000
            self.approved = int(amount)

    def validate(self):
        return True


#---------------------------------------------------------------------------
# Email verification views

def verify_email(request, email, infomodel):
    current_site = get_current_site(request)
    message = render_to_string('email_verification_email.html', {
        'user': infomodel,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(infomodel.pk)),
        'token': email_verification_token.make_token(infomodel),
    })
    subject = 'Confirm your email!'
    from_email = settings.EMAIL_HOST_USER
    to_list = [email,]
    email = EmailMessage(
        subject=subject,
        body=message,
        from_email=from_email,
        to=to_list,
        reply_to=[from_email],
        headers={'Content-Type': 'text/plain'},
    )
    print email.send()

def verify(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = InfoModel.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, InfoModel.DoesNotExist):
        user = None

    if user is not None and email_verification_token.check_token(user, token):
        user.email_verified = True
        #user.profile.email_confirmed = True
        user.save()
        #login(request, user)
        return redirect('index')
    else:
        return HttpResponse("Email verification failed!")


def activate_account(request, email, user):
    current_site = get_current_site(request)
    message = render_to_string('account_activation_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
    })
    subject = 'Activate your account!'
    from_email = settings.EMAIL_HOST_USER
    to_list = [email,]
    email = EmailMessage(
        subject=subject,
        body=message,
        from_email=from_email,
        to=to_list,
        reply_to=[from_email],
        headers={'Content-Type': 'text/plain'},
    )
    print email.send()


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.perkuser.web_info.email_verified = True
        user.save()
        #login(request, user)
        return redirect('index')
    else:
        return HttpResponse("Account Activation failed!")