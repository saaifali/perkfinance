from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from rest_framework import views
from rest_framework import permissions
from register.models import InfoModel, AppProfile
from register.views import activate_account
from register.api.serializers import RegisteredUserSerializer, UserSerializer, LoginParamSerializer, SuccessSerializer, SignUpParamSerializer, TokenSerializer, AppProfileSerializer
from rest_framework.response import Response
from rest_framework import status
import requests
from django.shortcuts import redirect
from register.models import PerkUser
from rest_framework.authtoken.models import Token

class RegisteredUserList(views.APIView):
    def get(self, request):
        users = InfoModel.objects.all()
        serializer = RegisteredUserSerializer(users, many=True)

        return Response(serializer.data)



class UserDetailView(views.APIView):
    model = InfoModel
    queryset = InfoModel.objects.all()
    serializer_class = RegisteredUserSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def post(self, request):
        params = LoginParamSerializer(data=request.data)
        if not params.is_valid():
            return Response(data=params.errors, status=status.HTTP_400_BAD_REQUEST)
        number = params.validated_data.get("number", None)
        email = params.validated_data.get("email", None)

        try:
            #user = InfoModel.objects.get(phone_number=number)
            user = InfoModel.objects.get(email=email)
            serializer = RegisteredUserSerializer(user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except InfoModel.DoesNotExist:
            data = {
                "exists": False
            }
            serializer = SuccessSerializer(data)
            return Response(serializer.data, status=status.HTTP_200_OK)



class LoginUserView(views.APIView):
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'email'
    permission_classes = [
        permissions.AllowAny
    ]

    def post(self, request):
        # json_data = request.data
        # print json_data
        # stream = BytesIO(json_data)
        # data = JSONParser.parse(stream)
        params = LoginParamSerializer(data=request.data)
        if not params.is_valid():
            return Response(data=params.errors, status=status.HTTP_400_BAD_REQUEST)
        email = params.validated_data.get("email", None)
        number = params.validated_data.get("number", None)
        mpin = params.validated_data.get("mpin", None)
        print "pin = " + mpin
        print email
        print number
        profile = None
        user = None
        data = {
            "exists": False
        }
        serializer = SuccessSerializer(data)
        if email:
            user = authenticate(username=email, password=mpin)
            if user is not None:
                login(request, user)
                token = Token.objects.get(user=user)
                token_serializer = TokenSerializer(token)
                return Response(data=token_serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(data=serializer.data, status=status.HTTP_200_OK)

        elif number:
            profile = InfoModel.objects.get(phone_number=number)
            user = authenticate(username=profile.email, password=mpin)
            if user is not None:
                login(request, user)
                token = Token.objects.get(user=user)
                token_serializer = TokenSerializer(token)
                return Response(data=token_serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(data=serializer.data, status=status.HTTP_200_OK)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)




class SetPinUserView(views.APIView):
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'email'
    permission_classes = [
        permissions.AllowAny
    ]

    def post(self,request):
        #json_data = request.data
        #print json_data
        #stream = BytesIO(json_data)
        #data = JSONParser.parse(stream)
        params = LoginParamSerializer(data = request.data)
        print params
        if not params.is_valid():
            return Response(data = params.errors, status=status.HTTP_404_NOT_FOUND)
        email = params.validated_data.get("email", None)
        number = params.validated_data.get("number", None)
        mpin = params.validated_data.get("mpin", None)
        print "pin = "+mpin
        print email
        print number
        if email:
            user = User.objects.create_user(username=email, email=email,password=mpin)
            #user.is_active = False
            user.save()
            user.refresh_from_db()
            user.save()
            activate_account(request, email, user)
            print authenticate(username = user.username, password=mpin)
            login(request, user)
            token = Token.objects.get(user = user)
            serializer = TokenSerializer(token)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        elif number:
            email = None
            try:
                profile = InfoModel.objects.get(phone_number=number)
                print profile
                email = profile.email
                try:
                    user = User.objects.get(email=email)
                    user.set_password(mpin)
                    user.save()
                    print user
                except User.DoesNotExist:
                    user = User.objects.create_user(username=email, email=email, password=mpin)
                    user.save()
            except InfoModel.DoesNotExist:
                data = {
                    "exists": False
                }
                serializer = SuccessSerializer(data)
                return Response(data=serializer.data , status=status.HTTP_400_BAD_REQUEST)
            user = authenticate(username=email, password=mpin)
            login(request, user)
            token = Token.objects.get(user=user)
            serializer = TokenSerializer(token)
            return Response(data=serializer.data, status=status.HTTP_202_ACCEPTED)


class SignupView(views.APIView):
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'email'
    permission_classes = [
        permissions.AllowAny
    ]

    def get(self,request):
        json_data = request.data
        print json_data
        #stream = BytesIO(json_data)
        #data = JSONParser.parse(stream)

        query_params = SignUpParamSerializer(data = request.query_params)
        if not query_params.is_valid():
            return Response(data = query_params.errors, status=status.HTTP_400_BAD_REQUEST)
        print query_params.validated_data
        email = query_params.validated_data.get("email")
        number = query_params.validated_data.get("number")
        user = None

        try:
            user = InfoModel.objects.get(email=email, phone_number=number)
        except InfoModel.DoesNotExist:
            serializer=None
            try:
                InfoModel.objects.get(phone_number=number)

                data = {
                    "exists":True
                }
                serializer = SuccessSerializer(data)
                return Response(serializer.data, status=status.HTTP_200_OK)
            except InfoModel.DoesNotExist:
                data = {
                    "exists":False
                }
                serializer = SuccessSerializer(data)
                #json_data = JSONRenderer(serializer.data)
            try:
                InfoModel.objects.get(email=email)
                data = {
                    "exists": True
                }
                serializer = SuccessSerializer(data)
                return Response(serializer.data, status=status.HTTP_200_OK)
            except InfoModel.DoesNotExist:
                #json_data = JSONRenderer(serializer.data)
                return Response(serializer.data, status=status.HTTP_200_OK)
        data = {
            "exists": True
        }
        serializer = SuccessSerializer(data)
        return Response(serializer.data, status=status.HTTP_200_OK)



class ProfileView(views.APIView):

    def post(self, request):
        profile = AppProfileSerializer(data=request.data, partial=True)
        if profile.is_valid():
            profile.save()
            return Response(status = status.HTTP_200_OK)
        return Response(status = status.HTTP_400_BAD_REQUEST)


    def get(self, request):
        pass



