from register.models import InfoModel, AppProfile
from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')

class RegisteredUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = InfoModel
        fields = ('phone_number',
                  'email',
                  'first_name',
                  'email_verified',
                  'last_name',
                  'gender',
                  'dob',
                  'pin',
                  'area',
                  'city',
                  'income',
                  'net_income',
                  'approved',
                  'amount_approved'
                  )


class LoginParamSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False)
    number = serializers.CharField(max_length=10, required=False)
    mpin = serializers.CharField(max_length=6, required=False)

class SignUpParamSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False)
    number = serializers.CharField(max_length=10, required=False)

class SuccessSerializer(serializers.Serializer):
    exists = serializers.BooleanField()

class ProfileSerializer(serializers.Serializer):
    pass

class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ('key',)

class AppProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppProfile
        fields = (
                  'first_name',
                  'last_name',
                  'gender',
                  'dob',
                  'pin',
                  'address',
                  'area',
                  'city',
                  'residing_since',
                  'home_type',
                  'education',
                  'college',
                  'college_city',
                  'income_type',
                  'total_experience',
                  'monthly_salary',
                  'current_emi',
                  'company',
                  'company_designation',
                  'company_pin',
                  'company_address',
                  'company_area',
                  'company_city',
                  )
