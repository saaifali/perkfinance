# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-07 10:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0004_auto_20170707_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='infomodel',
            name='net_income',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='infomodel',
            name='phone_number',
            field=models.IntegerField(default=0),
        ),
    ]
