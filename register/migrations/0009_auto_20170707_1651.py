# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-07 11:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0008_auto_20170707_1644'),
    ]

    operations = [
        migrations.AlterField(
            model_name='infomodel',
            name='first_name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='infomodel',
            name='gender',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='infomodel',
            name='last_name',
            field=models.CharField(max_length=100),
        ),
    ]
