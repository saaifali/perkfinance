# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-27 19:25
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0021_auto_20170827_1947'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='perkuser',
            name='mobile',
        ),
        migrations.AddField(
            model_name='perkuser',
            name='web_info',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='register.InfoModel'),
        ),
        migrations.AlterField(
            model_name='infomodel',
            name='first_name',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='infomodel',
            name='last_name',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='perkuser',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
