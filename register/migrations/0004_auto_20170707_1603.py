# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-07 10:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0003_auto_20170707_1538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='infomodel',
            name='phone_number',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
