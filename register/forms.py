from django import forms
from register.models import InfoModel

class ContactForm(forms.ModelForm):
    email = forms.EmailField(label="Email", widget=forms.EmailInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'example@foo.com'
        }
    ))

    phone_number = forms.DecimalField(max_digits=10, min_value=1111111111, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'We will not call you at night.'
        }
    ))
    check = forms.BooleanField(required=True, widget=forms.CheckboxInput())


    def clean(self):
        try:
            InfoModel.objects.get(phone_number=self.cleaned_data.get('phone_number'))
            raise forms.ValidationError("Number already exists!")
        except InfoModel.DoesNotExist:
            pass
        try:
            InfoModel.objects.get(email=self.cleaned_data['email'])
            raise forms.ValidationError("Email already exists!")
        except InfoModel.DoesNotExist:
            pass
        return self.cleaned_data

    class Meta:
        model = InfoModel
        fields = {'phone_number','email', 'check'}



class InfoForm(forms.ModelForm):
    #email = forms.EmailField(label="Email", widget=forms.HiddenInput())

    #phone_number = forms.DecimalField(max_digits=10, widget=forms.HiddenInput())

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'First Name'
        }
    ))
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Last Name'
        }
    ))
    CHOICES = [('M', 'Male'),
               ('F', 'Female')]
    gender = forms.ChoiceField(label='Gender', choices=CHOICES )
    dob = forms.DateField(label="Date of Birth", widget=forms.SelectDateWidget(years=range(2017,1970,-1), attrs={
        'class': 'form-control',
        }))
    pin = forms.DecimalField(label='Address Pin', max_digits=6, min_value=111111, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Your pincode please',
        }
    ))
    city = forms.CharField(label='City', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Mysore'
        }
    ))
    state = forms.CharField(label='State', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Karnataka'
        }
    ))

    INCOME_CHOICES = [('salaried', 'Salaried'),
               ('self', 'Self-employed')]


    income = forms.ChoiceField(label='', choices=INCOME_CHOICES)

    net_income = forms.IntegerField(label='Net monthly income',min_value=0, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': '500000'
        }
    ))




    class Meta:
        model = InfoModel
        fields = ('first_name', 'last_name', 'gender', 'dob', 'pin', 'city', 'state', 'income', 'net_income', )
